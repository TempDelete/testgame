﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CharacterController))]
public class Player : MonoBehaviour
{
    public static Player instance = null;

    [SerializeField] GameObject plant;
    [SerializeField] MeshRenderer capsule;

    [SerializeField] Vector3 direction = Vector3.forward;
    [SerializeField] float moveSpeed = 3.5f;

    CharacterController controller = null;
    bool isMoving = false;
    public bool canControl = true;
    public bool IsMoving => isMoving;
    void Start()
    {
        instance = this;
        controller = GetComponent<CharacterController>();
        direction.Normalize();
    }
    public void SetControlState(bool value)
    {
        canControl = value;
    }
    private void FixedUpdate()
    {
        if (!canControl) return;
        if (Input.touchCount > 0 || Input.GetMouseButton(0))
        {
            Move();
            plant.SetActive(false);
            capsule.enabled = true;
            isMoving = true;
        }
        else
        {
            capsule.enabled = false;
            plant.SetActive(true);
            isMoving = false;
        }
    }
    public void Move()
    {
        controller.Move(direction * moveSpeed * Time.fixedDeltaTime);
    }
}
