﻿using UnityEngine;
using System.Collections;

public class Guard : MonoBehaviour
{
    [SerializeField] float rotateSpeed = 2f;
    [SerializeField] float minTurnPeriod = 0.5f;
    [SerializeField] float maxTurnPeriod = 3f;
    [SerializeField] float visionAngle = 45f;

    [SerializeField] Vector3 EndRotation = Vector3.zero;

    Light light;
    RaycastHit hit;

    IEnumerator Start()
    {
        light = GetComponentInChildren<Light>();
        while(true)
        {
            yield return new WaitForSeconds(Random.Range(minTurnPeriod, maxTurnPeriod));
            var startRotation = transform.rotation;
            var endRotation = Quaternion.Euler(EndRotation);
            for (float i = 0; i <= 1; i += Time.deltaTime * rotateSpeed)
            {
                transform.rotation = Quaternion.Lerp(startRotation, endRotation, i);
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForSeconds(Random.Range(minTurnPeriod, maxTurnPeriod));
            for (float i = 0; i <= 1; i += Time.deltaTime * rotateSpeed)
            {
                transform.rotation = Quaternion.Lerp(endRotation, startRotation, i);
                yield return new WaitForEndOfFrame();
            }
        }
    }
    private void Update()
    {
        var angle = Vector3.Angle(transform.forward, Player.instance.transform.position - transform.position);
        if (Physics.Raycast(transform.position, Player.instance.transform.position - transform.position, out hit) &&
            angle <= visionAngle/2f)
        {
            if (Player.instance.IsMoving)
            {
                StopAllCoroutines();
                light.color = Color.red;
                GameManager.instance.LoseGame();
                enabled = false;
            }
        }
    }
}
