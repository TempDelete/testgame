﻿using UnityEngine;

public class NextRoom : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            GameManager.instance.WinGame();
        }
    }
}
