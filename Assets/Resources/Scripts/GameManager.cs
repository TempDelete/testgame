﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] UnityEvent onStartEvent = null;
    [SerializeField] UnityEvent onLoseEvent = null;
    [SerializeField] UnityEvent onWinEvent = null;

    public static GameManager instance = null;

    private void Start()
    {
        onStartEvent?.Invoke();
        instance = this;
    }
    public void WinGame()
    {
        onWinEvent?.Invoke();
    }
    public void LoseGame()
    {
        onLoseEvent?.Invoke();
    }
    public void OnRestartGame()
    {
        SceneManager.LoadScene(0);
    }
}
